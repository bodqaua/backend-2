const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

const apiPort = 3001;
const db = require('./db/db')
const router = require('./router/router')
app.use(express.static('public'))

app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());
app.use(bodyParser.json());

db.on('error', console.error.bind(console, 'MongoDB connection error:'))

app.use('/api', router);
// app.use(express.static('/assets'))

module.exports = db

app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`));
