const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Tag = new Schema({
    name: {type: String, required: true},
    ownerId: {type: String, required: true},
});

module.exports = mongoose.model('tag', Tag);