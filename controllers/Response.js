class Response {
    constructor(res, data) {
        res.status(200).json({success: true, ...data});
    }
}

class ErrorResponse {
    constructor(res, status, error, message) {
        res.status(status).json({success: true, error, message});
    }
}

module.exports = {Response, ErrorResponse};