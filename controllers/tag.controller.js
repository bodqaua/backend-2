const {Response, ErrorResponse} = require('./Response');
const TagService = require('../services/tag.service')

const getAllTags = async (req, res) => {
    const tags = await TagService.getTags();
    return new Response(res, {tags});
}

const newTag = async (req, res) => {
    const result = await TagService.newTag(req.body);
    if (!result.success) {
        return new ErrorResponse(res, 400, result.error, 'Something went wrong while saving tag');
    }

    return new Response(res, {message: 'Tag saved'});
}

const getTagsByOwner = async (req, res) => {
    const tags = await TagService.getTagByOwner(req.params.ownerId);
    return new Response(res, {tags});
}

const deleteTagById = async (req, res) => {
    const result = await TagService.deleteTagById(req.params.id);
    if (!result.success) {
        return new ErrorResponse(res, 400, result.error, 'Something went wrong while deleting tag');
    }

    return new Response(res, {message: "Success"});
}

const editTag = async(req, res) => {
    const result = await TagService.updateTag(req.body, req.params.id);
    if (!result.success) {
        return new ErrorResponse(res, 400, result.error, 'Something went wrong while editing tag');
    }

    return new Response(res, {message: 'Tag updated'});
}

const alphabeticallyTags = async (req, res) => {
    const tags = await TagService.getSortedTags({name: 1});
    return new Response(res, tags);
}

const searchTag = async(req, res) => {
    const query = {name: {"$regex": req.params.query, "$options": "i"}};
    const tags = await TagService.searchTag(query);
    return new Response(res, tags);
}

module.exports = {
    getAllTags,
    newTag,
    getTagsByOwner,
    deleteTagById,
    editTag,
    alphabeticallyTags,
    searchTag
}

