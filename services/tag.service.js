const Tag = require('../models/tag.model');

exports.getTags = async function () {
    return await Tag.find();
}

exports.newTag = async function (tag) {
    const newTag = new Tag(tag)

    return await newTag.save().then(() => {
        return {success: true}
    }).catch((error) => {
        return {success: false, error};
    })
}

exports.getTagByOwner = async function (ownerId) {
    return await Tag.find({ownerId: ownerId});
}

exports.updateTag = async function (tag, id) {
    return await Tag.findOneAndUpdate({_id: id}, tag)
        .then(() => {
            return {success: true}
        }).catch((error) => {
            return {success: false, error};
        })
}

exports.deleteTagById = async function (_id) {
    return await Tag.deleteOne({_id}).then(() => {
        return {success: true}
    }).catch((error) => {
        return {success: false, error};
    })
}

exports.deleteMultipleTags = async function (query) {
    return await Tag.deleteMany(query).then(() => {
        return {success: true}
    }).catch((error) => {
        return {success: false, error};
    })
}

exports.getSortedTags = async function (query, findQuery = {}) {
    return Tag.find(findQuery).sort(query);

}

exports.searchTag = async function (query) {
    return Tag.find(query);
}
