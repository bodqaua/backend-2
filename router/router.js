const express = require("express");

const TagController = require('../controllers/tag.controller');
const router = express.Router();

router.get('/tags', TagController.getAllTags);
router.get('/tags/owner/:ownerId', TagController.getTagsByOwner);
router.post('/tags', TagController.newTag);
router.delete('/tags/:id', TagController.deleteTagById)
router.put('/tags/:id', TagController.editTag)
router.get('/tags/sorted', TagController.alphabeticallyTags)
router.get('/tags/search/:query', TagController.searchTag)

module.exports = router;